# Soulmate.

There are prerequisites
to everthing in life.
Think about this.
Before you find your soul mate
You must first discover what ???
"Your" soul !!
What is the pre-req for Discovering
your Soul ?? -- Think

# What to pursue ?

Many things catch your eyes
But Can we go after all these things ????
No, we certainly cannot.
Then what to pursue?
Here is a indian heuristic
You have only one heart.
It can be captured by only one a few things
Persue only those things
that capture your heart
OK ?

# Start

Start where you are. Use what you have. Do what you can. - Arthur Ashe

# Courace

Sometimes all you need is a moment of extraordinary courage

# A Great Future

A great future doesn’t require a great past. - William Chapman

# Master

The master has failed more times than the beginner has even tried. - Steven McCraine

# vinU is a Pilot

The bad news is time flies, the good news is that you are the Pilot—Michael Althsuler

# Not moving fast enough

If you have everything under control, you are not moving fast enough - Mario Andretti

# Success

```80% is showing up```

# Priority

If you don't prioritize your life, someone else will.

# Do What You Can Do Well !!!

Don't let what you cannot do interfere with what you can do. - John Wooden


# Your Past

Every saint has a past. Every sinner has a future.

# vinU

If you are still looking for that one person who will change your life, 
take a look in the mirror.


# Ordinary

If you are not willing to risk the usual you will have to settle for the ordinary. - Jim Rohn


# Do it .

Do it with passion, or not at all.


# Be Humble 

Be humble, for you are of the earth. Be noble, for you are of the stars.

# OCD

Nothing diminishes anxiety faster than action.

# Resist Fear

Courage is resistance to fear, mastery of fear- not the absence of fear. Mark Twain

# Responsibility

No snowflake feels responsible for the avalanche.

# Opportunity

Opportunity is missed because it is dressed like overalls and looks like work.

# Great Way

If you cannot do great things, do small things in a great way.

# Truth

Dont confuse the truth
with the opinion of the majority

# Age

Count your age by friends, not years.
Count your life by smiles, not tears.

# Work

Nothing will work unless you do. - Maya Angelou

# Why Matters More 

What you do matters, but why you do it matters much more.

# Learn

Sometimes you win, sometimes you learn.

# Shortcuts

There are no short cuts to any place worth going.

# Strength

We either make ourselves miserable, or 
we make ourselves strong. The amount of work is the same.

# Risks

Risk more than others think is safe. Dream more than others think is practical. –Howard Schultz
